PHP-MVC
=========

MVC with PHP, MySQL and Bootstrap

## Installation
### Clone the Repository
    $ git clone https://bitbucket.org/Grek13/simple-mvc.git

### Chmod & chown directory for images
    $ sudo chmod 0777 -R assets/images/    
    $ sudo chown :www-data -R public/    

### Create the MySQL Database

Create database and import file frome /db/db.sql or just run following SQL code:

```sql
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(300) NOT NULL,
  `text` varchar(300) NOT NULL,
  `status` ENUM("0","1") NOT NULL DEFAULT "0",
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `task_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `path` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `login`, `password`) VALUES
(1, 'admin', '202cb962ac59075b964b07152d234b70');
```

### Login in admin as
    login:admin
    password:123

### Setup the `application/config/config.php` file like `application/config/config.php-sample`
```php
<?php

define('DB_HOST', 'localhost');
define('DB_NAME', 'DB_NAME');
define('DB_USER', 'DB_USER');
define('DB_PASSWORD', 'DB_PASSWORD');
define('HOME', 'http://' . $_SERVER['HTTP_HOST'] . '/');

```

<?php

class admin extends Controller {

	public $tasks;
	public $main_helper;

	public function index(){
		$this->checkLogin(FALSE);

		$this->tasks = $this->model('task');
		$this->main_helper = $this->helper('main_helper');
		$this->main_helper->additional_settings($this->tasks);

		$data = [
			'tasks'=>$this->tasks->getAllWithLimit(),
			'task_images'=>$this->model('task_image')->getMapAll('task_id', 'path'),
			'pagination_block'=>$this->main_helper->getPaginationBlock($this->tasks->count(), $this->tasks->limit),
		];

		$this->view('parts/header');
		$this->view('parts/menu');
		$this->view('admin/index', $data);
		$this->view('parts/footer');
	}

	public function login (){
		$this->checkLogin(TRUE, 'admin/index');

		if(!empty($_POST) && !empty($_POST)){
			$this->__postLogin();
		}

		$this->view('parts/header');
		$this->view('admin/login');
		$this->view('parts/footer');
	}

	public function logout(){
		Session::destroy();
		$this->redirect('admin/login');
	}

	protected function __postLogin(){
		$user = $this->model('user');
		if(!empty($_POST)){
			$login = $_POST['login'];
			$password = $_POST['password'];
			$userData = $user->getBy('login', $login);
			if (!empty($userData))
			{
				if (MD5($password) == $userData['0']['password'])
				{
					//set the session to true
					Session::set('loggin',true);
					// if matches login and password redirect to index
					$this->redirect('admin/index');
				}
				else
				{
					//when password is wrong
					$response = [];
					$response['message'][] = 'Your password is wrong, please try again !';
					$this->view('admin/login', $response);
				}
			}
			else
			{
				// when login is wrong
				$response = [];
				$response['message'][] = 'Your login is wrong, please try again !';
				$this->view('admin/login', $response);
			}
		}else{
			// if user press submit button without entering login and password
			$response = [];
			$response['message'][] = 'Please enter login and password to login !';
			$this->view('admin/login',$response);
		}
	}

	public function edit_task($id){
		$this->checkLogin(FALSE);
		$data = [
			'header' => "Edit task",
			'task' =>$this->model('task')->getById($id),
			'action' => 'edit',
		];

		$this->view('parts/header');
		$this->view('parts/menu');
		$this->view('task/form', $data);
		$this->view('parts/footer');
	}

	public function update_task(){
		$this->checkLogin(FALSE);
		if(!($post = $_POST))
			die('empty data');
		$response = [];
		if($this->model('task')->update($post['id'], ['status'=>!empty($post['status'])? $post['status'] : '0', 'text'=>$post['text']]) ){
			$response['message'][] = 'Your task has been updated !';
		}else{
			$response['message'][] = 'There is some problem, Please try again! !';
			$this->view('admin/edit_task/'.$post['id'],$response);
			exit;
		}

		$this->redirect('admin');
	}

	public function preview_task(){
		$this->checkLogin(FALSE);
		if(!($post = $_POST))
			die('empty data');
			
		$this->main_helper = $this->helper('main_helper');

		if($task = $this->model('task')->getById($post['id'])){
			$task['status'] = $post['status'];
			$task['text'] = $post['text'];
			$data = [
				'tasks'=>[$task],
				'task_images'=>$this->model('task_image')->getMapBy('task_id', $post['id'], 'task_id', 'path'),
			];
			$this->view('admin/preview', $data);
		}
	}

	private function checkLogin($loggin, $redirect='admin/login'){
		if(Session::get('loggin') == $loggin){
			$this->redirect($redirect);
		}
	}


}

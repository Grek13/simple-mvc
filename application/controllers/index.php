<?php
class index extends Controller
{

	private $task_images;
	private $tasks;
	public $main_helper;

	public function __construct(){
	}

	public function index (){
		$this->tasks = $this->model('task');
		$this->task_images = $this->model('task_image');
		$this->main_helper = $this->helper('main_helper');
		$this->main_helper->additional_settings($this->tasks);

		$data = [
			'tasks'=>$this->tasks->getAllWithLimit(),
			'task_images'=>$this->task_images->getMapAll('task_id', 'path'),
			'pagination_block'=>$this->main_helper->getPaginationBlock($this->tasks->count(), $this->tasks->limit),
		];

		$this->view('parts/header');
		$this->view('parts/menu');
		$this->view('index/index', $data);
		$this->view('parts/footer');
	}

	public function add_task(){
		$data = [
			'header' => "Add new task",
			'action' => 'add',
		];

		$this->view('parts/header');
		$this->view('parts/menu');
		$this->view('task/form', $data);
		$this->view('parts/footer');
	}

	public function save_task(){
		if(!($post = $_POST))
			die('empty data');
		$data = [];
		$response = [];
		$tasks = $this->model('task');
		$this->task_images = $this->model('task_image');

		$data['name'] = $post['name'];
		$data['email'] = $post['email'];
		$data['text'] = $post['text'];
		$images = $_FILES;

		$task_id = $tasks->save($data);

		if(!empty($task_id)){
			$response['message'][] = 'Your task has been saved !';
			if(!empty($images))
				$response['message'][] = $this->save_task_images($task_id, $images['images']);
		}else{
			$response['message'][] = 'There is some problem, Please try again! !';
			$this->view('index/add_task',$response);
			exit;
		}

		$this->redirect('index/index');
	}

	public function edit_task($id){
		echo 'edit sutdent of this id'.$id;
	}

	public function delete_task($id){
		echo 'delete sutdent of this id'.$id;
	}

	public function preview_task(){
		if(!($post = $_POST))
			die('empty data');


		$this->main_helper = $this->helper('main_helper');
		$task = [];
		$task[] = $post['status'];
		$task[] = $post['text'];
		$data = [
			'tasks'=>[
				'name'   => $post['name'],
				'email'	 => $post['email'],
				'status' => '0',
				'text'	 => $post['text'],
				'task_images'	 => $_FILES["images"]["tmp_name"],
			]
		];

		$this->view('index/preview', $data);

	}

	private function save_task_images($task_id, $images){
		if($this->task_images->save_all($task_id, $images))
			$message = "Your task's images has been saved !";
		else
			$message = "There is some problem with saving imsages";

		return $message;
	}

}

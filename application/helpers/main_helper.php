<?php

Class main_helper {

    public function getPaginationBlock($count, $limit){
		$pagination_block = "";
        $page = !empty($_GET['page'])? $_GET['page'] : 1;
        $href_end = $this->gePaginationHrefPart();

		if($count_pages = ceil($count / $limit)){
    		$previous = ($page > 1)? $page -1 : 1;
    		$previous_disabled = ($page == 1)? "disabled" : "";
    		$next_disabled = ($page == $count_pages)? "disabled" : "";
    		$next = ($page < $count_pages)? $page+1 : $count_pages;
			$pagination_block .= "<nav aria-label='...'><ul class='pagination justify-content-end'>";
			$pagination_block .= "<li class='previous {$previous_disabled}'><a href='?page={$previous}{$href_end}'>&larr; Previous</a></li>";
			for($i=1; $i<= $count_pages; ++$i){
				$active = ($page==$i)? "active" : "";
				$pagination_block .= "<li class='page-item {$active}'><a class='page-link' href='?page={$i}{$href_end}'>{$i}</a></li>";
			}
			$pagination_block .= "<li class='next {$next_disabled}'><a href='?page={$next}{$href_end}'>Next &rarr;</a></li>";
			$pagination_block .= "</ul></nav>";
		}

		return $pagination_block;
	}

    private function gePaginationHrefPart(){
        $href_end = "";
        if(!empty($_GET['order'])){
            $order_type = !empty($_GET['order_type'])? $_GET['order_type'] : 'asc';
            $href_end = "&order={$_GET['order']}&order_type={$order_type}";
        }
        return $href_end;
    }

    public function sortClass($get, $field){
        $class= "glyphicon glyphicon-sort";
        $get['order_type'] = !empty($get['order_type'])? $get['order_type'] : 'asc';

        if(!empty($get['order'])){
            switch ($get['order']) {
                case ($get['order'] == $field && $get['order_type'] == 'asc'):
                    $class .= '-by-attributes';
                    break;
                case ($get['order'] == $field && $get['order_type'] == 'desc'):
                    $class .= '-by-attributes-alt';
                    break;
            }
        }

        return $class;
    }

    public function getOrderType($get, $field){
        $type= "asc";
        $get['order_type'] = !empty($get['order_type'])? $get['order_type'] : 'asc';

        if(!empty($get['order'])){
            switch ($get['order']) {
                case ($get['order'] == $field && $get['order_type'] == 'asc'):
                    $type = 'desc';
                    break;
                case ($get['order'] == $field && $get['order_type'] == 'desc'):
                    $type = 'asc';
                    break;
            }
        }

        return $type;
    }

    public function make_images($images){
        $imgs = "";
        if(!empty($images)){
            foreach($images as $img){
                $imgs .= "<img class='task-image' src='{$img}' />";
            }
        }

        return $imgs;
    }

    public function additional_settings($model){
        $model->setOffset((!empty($_GET['page']) && $_GET['page'] >1)? ($_GET['page']-1)*$model->getLimit() : 0);
        if(!empty($_GET['order']) && !empty($_GET['order_type'])){
            $model->setOrder( $this->getOrderFieldNumber($model) );
            $model->setOrderType($_GET['order_type']);
        }
    }

    private function getOrderFieldNumber($model){
        $all_fields = $model->getFieldsTable('tasks');
        return !empty($all_fields[$_GET['order']])? $all_fields[$_GET['order']]+1 : 1 ;
    }
}

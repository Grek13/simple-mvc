<?php

Class Task_image extends Model
{
	protected $table = 'task_images';
	private $width=320;
	private $height=240;

	public function __construct(){
		parent::__construct();
	}

	public function save_all($task_id, $files){
		$result = FALSE;
        $uploaddir = DIR_ROOT.'/assets/images/'. $task_id. '/';
        $types = ['image/gif'=>TRUE, 'image/png'=>TRUE, 'image/jpg'=>TRUE, 'image/jpeg'=>TRUE];

        if( ! is_dir( $uploaddir ) ) {
            mkdir( $uploaddir, 0777 );
            chown ( realpath($uploaddir) , ':www-data' );
        }

        foreach( $files['name'] as $key=>$f ){
			$data = ['task_id'=>$task_id, 'path'=>'/assets/images/'. $task_id. '/'.$files['name'][$key]];
            if(!empty($types[$files['type'][$key]]) && move_uploaded_file( $files['tmp_name'][$key], $uploaddir . basename($files['name'][$key]))){
				$this->resizeImage(DIR_ROOT.$data['path']);
				if($this->save($data))
					$result = TRUE;
            }
        }

		return $result;
	}

	private function resizeImage($file_path){
		$size = getimagesize($file_path);
		if($size[0] > $this->width || $size[1] > $this->height){
			$ratio = $size[0]/$size[1];
			if( $ratio > 1) {
			    $width = $this->width;
			    $height = $this->width/$ratio;
			}else {
			    $width = $this->height*$ratio;
			    $height = $this->height;
			}

			$src = imagecreatefromstring(file_get_contents($file_path));
			$dst = imagecreatetruecolor($width,$height);
			$result = imagecopyresampled($dst,$src,0,0,0,0,$width,$height,$size[0],$size[1]);
			$result = imagedestroy($src);
			unlink($file_path);
			$result = imagepng($dst,$file_path);
			imagedestroy($dst);
		}
	}

}

<table class="table table-bordered table-striped table-hover">
<thead>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Status</th>
        <th>Text</th>
        <th>Images</th>
        <th>Actions</th>
    </tr>
<thead>
<tbody>
<?php
if(!empty($data['tasks'])){
    foreach($data['tasks'] as $k=>$t){
        $status = !empty($t['status'])? ['class'=>'approved','text'=>'Approved'] : ['class'=>'','text'=>'Not approved'];
        echo "<tr class='{$status['class']}'>";
        echo "<td>{$t['name']}</th>";
        echo "<td>{$t['email']}</th>";
        echo "<td>{$status['text']}</th>";
        echo "<td>{$t['text']}</th>";
        echo "<td>".$this->main_helper->make_images($data['task_images'][$t['id']])."</td>";
        echo "<td><a href='/admin/edit_task/{$t['id']}'>Edit</a></th>";
        echo "</tr>";
    }
}
?>
    </tbody>
</table>

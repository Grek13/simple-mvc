<?php

if (!empty($data['message'])){
    echo "<ul>";
    foreach($data['message'] as $message)
        echo "<li>{$message}</li>";
    echo "</ul>";
}
?>
<div class="conteiner">
    <h3>Tasks</h3>
    <table class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th><a class="sort sort_field" href="?page=<?php echo !empty($_GET['page'])? $_GET['page'] : 1;?>&order=name&order_type=<?php echo $this->main_helper->getOrderType($_GET, 'name')?>">Name <span class="<?php echo $this->main_helper->sortClass($_GET, 'name')?>"></span></a></th>
            <th><a class="sort sort_field" href="?page=<?php echo !empty($_GET['page'])? $_GET['page'] : 1;?>&order=email&order_type=<?php echo $this->main_helper->getOrderType($_GET, 'email')?>">Email <span class="<?php echo $this->main_helper->sortClass($_GET, 'email')?>"></span></a></th>
            <th><a class="sort sort_field" href="?page=<?php echo !empty($_GET['page'])? $_GET['page'] : 1;?>&order=status&order_type=<?php echo $this->main_helper->getOrderType($_GET, 'status')?>">Status <span class="<?php echo $this->main_helper->sortClass($_GET, 'status')?>"></span></a></th>
            <th>Text</th>
            <th>Images</th>
        </tr>
    <thead>
    <tbody>
    <?php
    if(!empty($data['tasks'])){
        foreach($data['tasks'] as $k=>$t){
            $status = !empty($t['status'])? ['class'=>'approved','text'=>'Approved'] : ['class'=>'','text'=>'Not approved'];
            echo "<tr class='{$status['class']}'>";
            echo "<td>{$t['name']}</th>";
            echo "<td>{$t['email']}</th>";
            echo "<td>{$status['text']}</th>";
            echo "<td>{$t['text']}</th>";
            echo "<td>".(!empty($data['task_images'][$t['id']])? $this->main_helper->make_images($data['task_images'][$t['id']]) : '')."</td>";
            echo "</tr>";
        }
    }
    ?>
    </tbody>
</table>
<?php echo !empty($data['pagination_block'])? $data['pagination_block'] : ''; ?>
</div>

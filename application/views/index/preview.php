<table class="table table-bordered table-striped table-hover">
<thead>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Status</th>
        <th>Text</th>
        <th>Images</th>
    </tr>
<thead>
<tbody>
<?php
if(!empty($data['tasks'])){
    $status = !empty($data['tasks']['status'])? ['class'=>'approved','text'=>'Approved'] : ['class'=>'','text'=>'Not approved'];
    echo "<tr class='{$status['class']}'>";
    echo "<td>{$data['tasks']['name']}</th>";
    echo "<td>{$data['tasks']['email']}</th>";
    echo "<td>{$status['text']}</th>";
    echo "<td>{$data['tasks']['text']}</th>";
    echo "<td id='image_preview'></td>";
    echo "</tr>";
}
?>
    </tbody>
</table>

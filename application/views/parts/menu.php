<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">

      <a class="navbar-brand <?php echo (empty($_GET['url']) || $_GET['url'] == '/')? 'active' : ''?>" href="/">Home</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="<?php echo (!empty($_GET['url']) && $_GET['url'] == 'index/add_task')? 'active' : ''?>" ><a href="/index/add_task">Add task</a></li>
		<li class="<?php echo (!empty($_GET['url']) && strstr($_GET['url'], 'admin'))? 'active' : ''?>" ><a href="/admin">Admin</a></li>
      </ul>
	  <?php
	  if(Session::get('loggin') == TRUE){
	  ?>
      <ul class="nav navbar-nav navbar-right">
        <li><a class="btn" href="/admin/logout">Logout</a></li>
      </ul>
	  <?php } ?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

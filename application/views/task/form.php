<?php
	if (!empty($data['message'])){
    ?>
	<ul>
		<?php
		foreach($data['message'] as $message){
        ?>
			<li> <?php echo $message; ?> </li>
		<?php } ?>
	</ul>
	<?php } ?>
<div class="conteiner">
	<h2><?php echo !empty($data['header'])? $data['header'] : ''?></h2>
	<form id="task_form" role="form" action="<?php echo ($data['action']!='edit')? HOME.'index/save_task' : HOME.'admin/update_task'; ?>" method="post"  enctype="multipart/form-data">
		<?php
		if($data['action']=='edit'){?>
			<input id="task_id" type="hidden" name="id" value="<?php echo !empty($data['task']['id'])? $data['task']['id'] : ''?>" />
		<?php } ?>
	    <div>
	    	<label for="name">Name</label>
	    	<input  id="name" class="form-control" name="name" placeholder="Jhon Doe" required="required" type="text" value="<?php echo !empty($data['task']['name'])? $data['task']['name'] : ''?>" <?php echo ($data['action']=='edit')? "disabled" : '' ?> />
	    </div>
	    <div>
	    	<label for="email">Email</label>
	    	<input  id="email" class="form-control" name="email" placeholder="user@mail.com" required="required" type="email" value="<?php echo !empty($data['task']['email'])? $data['task']['email'] : ''?>" <?php echo ($data['action']=='edit')? "disabled" : '' ?> />
	    </div>
	    <div>
	    	<label for="name">Text</label>
	        <textarea  id="text" class="form-control" name="text" placeholder="Task's text" required="required" type="text"><?php echo !empty($data['task']['text'])? $data['task']['text'] : ''?></textarea>
	    </div>
		<?php
		if($data['action']=='edit'){?>
		<div class="checkbox">
			<label><input id="task_status" name="status" type="checkbox" value="1" <?php echo (!empty($data['task']['status']) && $data['task']['status'] == '1')? 'checked' : ''?>>Status</label>
		</div>
		<?php } ?>
		<?php
		if($data['action']!='edit'){
		?>
	    <div class="img_div">
	    	<label for="images">Add images</label>
	    	<input id="image_task" name="images[]" class="form-control" type="file" multiple="multiple"  accept="image/gif, image/png, image/jpg, image/jpeg" />
	    </div>
		<?php } ?>
		<div >
	    	<button type="submit" id="btn_add" class="btn btn-primary">Submit</button>
		</div>
	</form>

	<div >
		<button type="submit" id="<?php echo $data['action']=='edit'? 'preview_admin' : 'preview_task' ?>" class="btn btn-primary">Preview</button>
	</div>
	<div style="clear:both;"></div>
	<div id="preview_block"></div>
</div>

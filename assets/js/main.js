$(document).on('click', '#preview_admin', function(){
    var status = $('#task_status').is(':checked') == true? '1' : '0';
    $.ajax({
        type: "POST",
        data: {id: $('#task_id').val(), text: $('#text').val(), status:status},
        url: "/admin/preview_task",
        success: function (response) {
            $('#preview_block').html(response);
        }
    });
})

$(document).on('click', '#preview_task', function(){
    var formData = new FormData($("#task_form").get(0));
    $.ajax({
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        url: "/index/preview_task",
        success: function (response) {
            $('#preview_block').html(response);
            showImages();
        }
    });
})

function showImages() {
    $( document.getElementById("image_task").files ).each(function( index, value ) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(value);
        oFReader.onload = function (oFREvent) {
            var img = $('<img>');
            img.attr('src', oFREvent.target.result);
            img.appendTo('#image_preview');
        };
    });
}

<?php

require_once DIR_ROOT.'/application/config/config.php';
require_once DIR_ROOT.'/application/config/session.php';
require_once DIR_ROOT.'/system/core.php';
require_once DIR_ROOT.'/system/controller.php';
require_once DIR_ROOT.'/system/model.php';

Session::init();

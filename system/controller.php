<?php
class Controller
{


	public function model($model)
	{
		if (file_exists(DIR_ROOT.'/application/models/' . $model . '.php'))
		{
			require_once DIR_ROOT.'/application/models/' . $model . '.php';
			return new $model;
		}
	}


	public function view($view , $data = [])
	{
		if (file_exists(DIR_ROOT.'/application/views/' . $view . '.php'))
		{
			require_once DIR_ROOT.'/application/views/' . $view . '.php';
		}
	}


	public function helper($helper)
	{
		if (file_exists(DIR_ROOT.'/application/helpers/' . $helper . '.php'))
		{
			require_once DIR_ROOT.'/application/helpers/' . $helper . '.php';
			return new $helper;
		}
	}


	public function redirect($path)
	{
		$fullPath = HOME.$path;
		header("Location: $fullPath");
	}


}

<?php
class Core
{
	protected $controller = 'index';
	protected $method = 'index';
	protected $params = [];

	public function __construct()
	{
		$url = $this->parseUrl();
		// check controller is available or not
		if (file_exists(DIR_ROOT.'/application/controllers/' . $url[0] . '.php'))
		{
			$this->controller = $url[0];
			unset($url[0]);
		}
		//include it if available
		require_once DIR_ROOT.'/application/controllers/' . $this->controller . '.php';

		$this->controller = new $this->controller();

		if (!empty($url[1]) && method_exists($this->controller, $url[1]))
		{
			$this->method = $url[1];
				unset($url[1]);
		}

		$this->params = !empty($url) ? array_values($url) : [];

		call_user_func_array([$this->controller, $this->method], $this->params);
	}

	public function parseUrl()
	{
		if (!empty($_GET['url']))
		{
			return explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
		}
	}
}

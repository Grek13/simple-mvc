<?php
class Model
{

	protected $db;
	protected $table = "";
	public $offset = 0;
	public $limit = 3;
	protected $sth;
	public $order = '1';
	public $orderType = 'asc';

	public function __construct(){
		$dns = 'mysql:dbname=' . DB_NAME . ";host=" . DB_HOST;
		try {
			$this->db = new PDO($dns, DB_USER, DB_PASSWORD);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			echo $e->getMessage();
			exit();
		}
	}

	public function getAll(){
		try {
			$this->sth = $this->db->prepare("SELECT * FROM $this->table ");
			$this->sth->execute();
			return $this->sth->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e){
			return [];
		}
	}

	public function getAllWithLimit(){
		try {
			$this->sth = $this->db->prepare("SELECT * FROM $this->table ORDER BY :order {$this->orderType} LIMIT :offset, :limit");
			$this->paginate();
			$this->bindOrder();
			$this->sth->execute();
			return $this->sth->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e){
			return [];
		}
	}

	public function count(){
		try {
			return (int)$this->db->query("SELECT COUNT(*) FROM $this->table")->fetchColumn();
		} catch (PDOException $e)
		{
			return 0;
		}
	}

	public function getById($id = null){
		try {
			$this->sth = $this->db->prepare("SELECT * FROM $this->table WHERE id = ?");
			$this->sth->execute(array($id));
			return $this->sth->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $e)
		{
		return false;
		}
	}

	public function getBy($columnName, $value, $count = false){
		try
		{
			$this->sth = $this->db->prepare("SELECT * FROM $this->table WHERE $columnName = :columnValue");
			$this->sth->bindParam(':columnValue', $value, PDO::PARAM_STR);
			$this->sth->execute();
			$resultSet = $this->sth->fetchAll(PDO::FETCH_ASSOC);
			if ($count)
			{
				return count($resultSet);
			}
			return $resultSet;
		} catch (PDOException $e){
			return FALSE;
		}
	}

	public function save($data = []){
		$fields = array_keys($data); // here you have to trust your field names!
		$values = array_values($data);
		$fieldlist = implode(',', $fields);
		$qs = str_repeat("?,", count($fields) - 1);
		$sql = "insert into $this->table($fieldlist) values(${qs}?)";
		try {
			$this->sth = $this->db->prepare($sql);
			$this->sth->execute($values);
			return $this->db->lastInsertId();
		} catch (PDOException $e){
			return FALSE;
		}
	}

	public function delete($id = null){
		try {
			$this->sth = $this->db->prepare("DELETE FROM $this->table WHERE id = ? LIMIT 1");
			return $this->sth->execute(array($id));
		} catch (PDOException $e)
		{
		return false;
		}
	}

	public function update($id = null, $data = []){
		$temp = [];
		foreach (array_keys($data) as $name) {
			$temp[] = "`$name` = ?";
		}

		$setPara = implode(', ', $temp);
		$sql = "UPDATE $this->table SET $setPara WHERE id = ?";
		$queryData = array_values($data);
		$queryData[] = $id;
		try
		{


			$stmt = $this->db->prepare($sql);
			return $stmt->execute($queryData);
		} catch (PDOException $e)
		{
			return false;
		}
	}

	public function getMapAll($key, $field=''){
		$result = [];
		if($data = $this->getAll())
			foreach($data as $v)
				$result[$v[$key]][] = !empty($field)? $v[$field] : $v;

		return $result;
	}

	public function getMapBy($columnName, $value, $key, $field=''){
		$result = [];
		if($data = $this->getBy($columnName, $value))
			foreach($data as $v)
				$result[$v[$key]][] = !empty($field)? $v[$field] : $v;

		return $result;
	}

	public function getFieldsTable($table){
		$this->sth = $this->db->prepare("DESCRIBE {$table}");
		$this->sth->execute();
		return array_flip($this->sth->fetchAll(PDO::FETCH_COLUMN));
	}

	private function paginate(){
		$this->sth->bindValue(':offset', (int) $this->offset, PDO::PARAM_INT);
		$this->sth->bindValue(':limit', (int) $this->limit, PDO::PARAM_INT);
	}

	public function setOffset($offset){
		$this->offset = $offset;
	}

	public function setLimit($limit){
		$this->limit = $limit;
	}

	public function setOrder($order){
		$this->order = $order;
	}

	public function setOrderType($orderType){
		$types = ['asc'=>'asc', 'desc'=>'desc'];
		$this->orderType = !empty($types[$orderType])? $types[$orderType] : $types['asc'];
	}

	public function getOffset(){
		return $this->offset;
	}

	public function getLimit(){
		return $this->limit;
	}

	public function bindOrder(){
		$this->sth->bindParam(':order', $this->order, PDO::PARAM_INT);
	}

}
